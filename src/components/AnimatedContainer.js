import React, { useState } from "react";
import TimeControl from "./TimeControl";

const STEP = 120000; // 2 minutes
const INTERVAL = 250; // 0.25 sec
const DELTA_START = 3600000; // 1 hour
const DELTA_END = 3600000; // 1 hour

function AnimatedContainer({ originalTime }) {
  const [animationTime, setAnimationTime] = useState(originalTime);
  const startTime = new Date(originalTime.getTime() - DELTA_START);
  const endTime = new Date(originalTime.getTime() + DELTA_END);
  return (
    <div>
      <fieldset>
        <legend>Time control</legend>

        <TimeControl
          startTime={startTime}
          endTime={endTime}
          step={STEP}
          interval={INTERVAL}
          originalTime={originalTime}
          onUpdateAnimationTime={setAnimationTime}
        />
      </fieldset>
      <fieldset>
        <legend>Main content</legend>
        <h1>Now showing: {animationTime.toISOString()}</h1>
      </fieldset>
    </div>
  );
}

export default AnimatedContainer;
