import "leaflet/dist/leaflet.css";
import "./LeafletMapPrototype.css";
import React from "react";
import { ImageOverlay, MapContainer, TileLayer } from "react-leaflet";

const OVERLAY_TIMESTAMPS = [
  "202012111000",
  "202012111030",
  "202012111100",
  "202012111130",
  "202012111200",
  "202012111230",
  "202012111300",
  "202012111330",
  "202012111400",
  "202012111430",
  "202012111500",
  "202012111530",
  "202012111600",
  "202012111630",
  "202012111700",
  "202012111730",
  "202012111800",
];

const ANIMATION_INTERVAL = 250;

const getUrl = (timestamp) => `http://localhost:8000/${timestamp}.png`;

// Naive preploading. Consider this if we need to wait for all images: https://jack72828383883.medium.com/how-to-preload-images-into-cache-in-react-js-ff1642708240
function preloadImages() {
  OVERLAY_TIMESTAMPS.forEach((timestamp) => {
    const img = new Image();
    img.src = getUrl(timestamp);
  });
}

export default class LeafletMapPrototype extends React.Component {
  animationIntervalId = null;

  state = {
    frameIndex: 0,
    isPlaying: false,
  };

  componentDidMount = () => {
    console.log("MOUNTED");
    preloadImages();
  };

  componentWillUnmount = () => {
    console.log("CLEANING UP");
    clearInterval(this.animationIntervalId);
  };

  play = () => {
    this.animationIntervalId = setInterval(
      this.goToNextFrame,
      ANIMATION_INTERVAL
    );
    this.setState({ isPlaying: true });
  };

  pause = () => {
    clearInterval(this.animationIntervalId);
    this.setState({ isPlaying: false });
  };

  goToNextFrame = () => {
    this.setState((prevState) => {
      const { frameIndex } = prevState;
      const nextIndex = (frameIndex + 1) % OVERLAY_TIMESTAMPS.length;
      return { frameIndex: nextIndex };
    });
  };

  handleSelectFrame = (event) =>
    this.setState({ frameIndex: event.target.selectedIndex });

  render = () => {
    const { frameIndex, isPlaying } = this.state;
    const overlayUrl = getUrl(OVERLAY_TIMESTAMPS[frameIndex]);
    return (
      <div>
        <button onClick={this.goToNextFrame}>Next frame</button>
        {isPlaying ? (
          <button onClick={this.pause}>Pause</button>
        ) : (
          <button onClick={this.play}>Play</button>
        )}
        <select
          size={10}
          onChange={this.handleSelectFrame}
          value={OVERLAY_TIMESTAMPS[frameIndex]}
        >
          {OVERLAY_TIMESTAMPS.map((timestamp) => (
            <option key={timestamp} value={timestamp}>
              {timestamp}
            </option>
          ))}
        </select>
        <MapContainer center={[55.505, 11.09]} zoom={5} scrollWheelZoom={true}>
          <TileLayer
            attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
            url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
          />
          <ImageOverlay
            opacity={0.6}
            bounds={[
              {
                lat: 54,
                lon: 6,
              },
              {
                lat: 58,
                lon: 17,
              },
            ]}
            url={overlayUrl}
          />
        </MapContainer>
      </div>
    );
  };
}
