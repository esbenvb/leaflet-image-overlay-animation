import { storiesOf } from "@storybook/react";
import React from "react";
import LeafletMapWithTimeControl from "./LeafletMapWithTimeControl";

storiesOf("LeafletMapWithTimeControl", module).add("default", () => (
  <LeafletMapWithTimeControl
    originalTime={new Date("2020-12-11T16:00:00+01:00")} // This should be properly handled with time zones etc.
  />
));
