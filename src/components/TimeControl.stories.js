import { storiesOf } from "@storybook/react";
import React from "react";
import TimeControl from "./TimeControl";
import { action } from "@storybook/addon-actions";

storiesOf("TimeControl", module).add("default", () => (
  <TimeControl
    interval={250}
    step={120000}
    startTime={new Date("2020-12-22T10:57:32.634Z")}
    endTime={new Date("2020-12-22T12:57:32.634Z")}
    originalTime={new Date("2020-12-22T11:30:32.634Z")}
    onUpdateAnimationTime={action("onUpdateAnimationTime")}
  />
));
