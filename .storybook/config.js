import { configure } from '@storybook/react';

function loadStories() {
  const req = require.context('../src', true, /\.stories\.js$/);
  req.keys().forEach(fname => req(fname)); 
}

configure(loadStories, module);
