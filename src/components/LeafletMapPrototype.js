import "leaflet/dist/leaflet.css";
import "./LeafletMapPrototype.css";
import React, { useEffect, useRef, useState } from "react";
import { ImageOverlay, MapContainer, TileLayer } from "react-leaflet";

const OVERLAY_TIMESTAMPS = [
  "202012111000",
  "202012111030",
  "202012111100",
  "202012111130",
  "202012111200",
  "202012111230",
  "202012111300",
  "202012111330",
  "202012111400",
  "202012111430",
  "202012111500",
  "202012111530",
  "202012111600",
  "202012111630",
  "202012111700",
  "202012111730",
  "202012111800",
];

const ANIMATION_INTERVAL = 250;

const getUrl = (timestamp) => `http://localhost:8000/${timestamp}.png`;

// Naive preploading. Consider this if we need to wait for all images: https://jack72828383883.medium.com/how-to-preload-images-into-cache-in-react-js-ff1642708240
function preloadImages() {
  OVERLAY_TIMESTAMPS.forEach((timestamp) => {
    const img = new Image();
    img.src = getUrl(timestamp);
  });
}

const usePrevious = (value, initialValue) => {
  const ref = useRef(initialValue);
  useEffect(() => {
    ref.current = value;
  });
  return ref.current;
};

function LeafletMapPrototype() {
  const animationIntervalIdRef = useRef(null);

  useEffect(() => {
    console.log("MOUNTED");
    preloadImages();
    return () => {
      console.log("CLEANING UP");
      clearInterval(animationIntervalIdRef.current);
    };
  }, []);

  const [frameIndex, setFrameIndex] = useState(0);
  const [isPlaying, setIsPlaying] = useState(false);
  const previousIsPlaying = usePrevious(isPlaying);

  useEffect(() => {
    if (isPlaying && !previousIsPlaying) {
      console.log("starting playback", { isPlaying, previousIsPlaying });
      let currentFrameIndex = frameIndex // hack to keep newest value
      animationIntervalIdRef.current = setInterval(() => {
        const nextIndex = (currentFrameIndex + 1) % OVERLAY_TIMESTAMPS.length; // hack to keep newest value
        setFrameIndex(nextIndex);
        currentFrameIndex = nextIndex // hack to keep newest value
      }, ANIMATION_INTERVAL);
    } else if (!isPlaying && previousIsPlaying) {
      console.log("stopping playback", { isPlaying, previousIsPlaying });
      clearInterval(animationIntervalIdRef.current);
    }
  }, [isPlaying, previousIsPlaying, frameIndex]);

  const overlayUrl = getUrl(OVERLAY_TIMESTAMPS[frameIndex]);
  return (
    <div>
      <button
        onClick={() =>
          setFrameIndex((frameIndex + 1) % OVERLAY_TIMESTAMPS.length)
        }
      >
        Next frame
      </button>
      {isPlaying ? (
        <button onClick={() => setIsPlaying(false)}>Pause</button>
      ) : (
        <button onClick={() => setIsPlaying(true)}>Play</button>
      )}
      <select
        size={10}
        onChange={(event) => setFrameIndex(event.target.selectedIndex)}
        value={OVERLAY_TIMESTAMPS[frameIndex]}
      >
        {OVERLAY_TIMESTAMPS.map((timestamp) => (
          <option key={timestamp} value={timestamp}>
            {timestamp}
          </option>
        ))}
      </select>
      <MapContainer center={[55.505, 11.09]} zoom={5} scrollWheelZoom={true}>
        <TileLayer
          attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
          url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
        />
        <ImageOverlay
          opacity={0.6}
          bounds={[
            {
              lat: 54,
              lon: 6,
            },
            {
              lat: 58,
              lon: 17,
            },
          ]}
          url={overlayUrl}
        />
      </MapContainer>
    </div>
  );
}

export default LeafletMapPrototype;
