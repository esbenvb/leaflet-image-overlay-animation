import "leaflet/dist/leaflet.css";
import "./LeafletMapPrototype.css";
import { MapContainer, TileLayer } from "react-leaflet";

import React, { useState } from "react";
import TimeControl from "./TimeControl";
import TimestampImageOverlay from "./TimestampImageOverlay";

const STEP = 1800000; // 30 minutes
const INTERVAL = 250; // 0.25 sec
const DELTA_START = 21600000; // 6 hours
const DELTA_END = 7200000; // 2 hours

function LeafletMapWithTimeControl({ originalTime }) {
  const [animationTime, setAnimationTime] = useState(originalTime);
  const startTime = new Date(originalTime.getTime() - DELTA_START);
  const endTime = new Date(originalTime.getTime() + DELTA_END);
  return (
    <div>
      <fieldset>
        <legend>Time control</legend>

        <TimeControl
          startTime={startTime}
          endTime={endTime}
          step={STEP}
          interval={INTERVAL}
          originalTime={originalTime}
          onUpdateAnimationTime={setAnimationTime}
        />
      </fieldset>
      <fieldset>
        <legend>Main content</legend>
        <h1>Now showing: {animationTime.toISOString()}</h1>
        <MapContainer center={[55.505, 11.09]} zoom={5} scrollWheelZoom={true}>
          <TileLayer
            attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
            url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
          />
          <TimestampImageOverlay
            opacity={0.6}
            bounds={[
              {
                lat: 54,
                lon: 6,
              },
              {
                lat: 58,
                lon: 17,
              },
            ]}
            time={animationTime}
          />
        </MapContainer>
      </fieldset>
    </div>
  );
}

export default LeafletMapWithTimeControl;
