import { storiesOf } from "@storybook/react";
import React from "react";
import AnimatedContainer from "./AnimatedContainer";

storiesOf("AnimatedContainer", module)
  .add("now", () => (
    <AnimatedContainer originalTime={new Date()}></AnimatedContainer>
  ))
  .add("2020-12-22", () => (
    <AnimatedContainer
      originalTime={new Date("2020-12-22T12:21:09.501Z")}
    ></AnimatedContainer>
  ))
  .add("2020-02-22", () => (
    <AnimatedContainer originalTime={new Date("2020-02-22T12:21:09.501Z")}></AnimatedContainer>
  ));
