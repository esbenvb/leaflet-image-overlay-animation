import React, { useEffect } from "react";
import { ImageOverlay } from "react-leaflet";

const OVERLAY_TIMESTAMPS = [
  "202012111000",
  "202012111030",
  "202012111100",
  "202012111130",
  "202012111200",
  "202012111230",
  "202012111300",
  "202012111330",
  "202012111400",
  "202012111430",
  "202012111500",
  "202012111530",
  "202012111600",
  "202012111630",
  "202012111700",
  "202012111730",
  "202012111800",
];

const getUrl = (timestamp) => `http://localhost:8000/${timestamp}.png`;

function preloadImages() {
  OVERLAY_TIMESTAMPS.forEach((timestamp) => {
    const img = new Image();
    img.src = getUrl(timestamp);
  });
}

// Get the latest timestamp that is older or equal to the current formatted time
const findBestMatch = (formattedTime, availableTimes) =>
  availableTimes
    .sort() // remove this one if the API already returns sorted data
    .reverse()
    .find((item) => item <= formattedTime);

function formatTime(t) {
  // https://www.codegrepper.com/code-examples/javascript/date+to+string+format+javascript
  const date = ("0" + t.getDate()).slice(-2);
  const month = ("0" + (t.getMonth() + 1)).slice(-2);
  const year = t.getFullYear();
  const hours = ("0" + t.getHours()).slice(-2);
  const minutes = ("0" + t.getMinutes()).slice(-2);
  // This should be properly handled with time zones etc.
  return `${year}${month}${date}${hours}${minutes}`;
}

function TimestampImageOverlay({ time, opacity, bounds }) {
  // add `type prop here if needed for different types of overlays with different sources of avaliable time stamps etc

  useEffect(() => {
    preloadImages();
  }, []);

  // Format the time to use the DMI proprietary format
  const formattedTime = formatTime(time);
  // Find the best matching timestamp
  const bestMatch = findBestMatch(formattedTime, OVERLAY_TIMESTAMPS);
  // Convert it to a valid image URL
  const url = getUrl(bestMatch);
  return <ImageOverlay opacity={opacity} bounds={bounds} url={url} />;
}

export default TimestampImageOverlay;
