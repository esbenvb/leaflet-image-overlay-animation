import { storiesOf } from "@storybook/react";
import React from "react";
import LeafletMapPrototypeClass from "./LeafletMapPrototypeClass";

storiesOf("LeafletMapPrototypeClass", module).add("default", () => (
  <LeafletMapPrototypeClass />
));
