import React from "react";

export default class TimeControl extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isPlaying: false,
      animationTime: props.originalTime,
    };
  }

  intervalId = null;

  componentDidMount = () => {};

  componentDidUpdate = (prevProps, prevState) => {
    const { animationTime } = this.state;
    const { onUpdateAnimationTime, originalTime, endTime } = this.props;
    // Detect changes and report new animationTime every time it changes
    if (prevState.animationTime !== animationTime) {
      onUpdateAnimationTime(animationTime);
      if (animationTime.getTime() === endTime.getTime()) {
        this.handlePause();
      }
    }
    // If new originalTime is set, stop animation and adjust to presentation time.
    if (prevProps.originalTime !== originalTime) {
      this.handleToCurrent();
    }
  };

  componentWillUnmount = () => {
    // clean up
    clearInterval(this.intervalId);
  };

  handleToStart = () => {
    const { startTime } = this.props;
    this.setState({ animationTime: startTime });
  };
  handleToEnd = () => {
    const { endTime } = this.props;
    this.setState({ animationTime: endTime });
  };

  handleForward = () => {
    const { endTime, step } = this.props;
    this.setState((prevState) => {
      const { animationTime } = prevState;
      // Make sure we don't exceed the endTime
      const newTime = Math.min(
        animationTime.getTime() + step,
        endTime.getTime()
      );
      return {
        // Convert the newly calculated time to a date object
        animationTime: new Date(newTime),
      };
    });
  };

  handleBackward = () => {
    const { startTime, step } = this.props;
    this.setState((prevState) => {
      const { animationTime } = prevState;
      // Make sure we don't exceed the endTime
      const newTime = Math.max(
        animationTime.getTime() - step,
        startTime.getTime()
      );
      return {
        // Convert the newly calculated time to a date object
        animationTime: new Date(newTime),
      };
    });
  };

  handlePlay = () => {
    const { interval, endTime, startTime } = this.props;
    const { animationTime } = this.state;

    // Reset time to start if pressing play at the end
    if (animationTime.getTime() === endTime.getTime()) {
      this.setState({ isPlaying: true, animationTime: startTime });
    } else {
      this.setState({ isPlaying: true });
    }
    this.intervalId = setInterval(this.handleForward, interval);
  };

  handlePause = () => {
    clearInterval(this.intervalId);
    this.setState({ isPlaying: false });
  };

  handleToOriginal = () => {
    clearInterval(this.intervalId);
    const { originalTime } = this.props;
    this.setState({
      isPlaying: false,
      animationTime: originalTime,
    });
  };

  handleSetAnimationTime = (event) => {
    this.setState({ animationTime: new Date(parseInt(event.target.value)) });
  };

  render = () => {
    const {
      originalTime,
      startTime,
      endTime,
      step,
      interval,
      onUpdateAnimationTime,
    } = this.props;
    const { isPlaying, animationTime } = this.state;

    return (
      <div style={{ flexDirection: "column" }}>
        <input
          type="range"
          list="tickmarks"
          min={startTime.getTime()}
          max={endTime.getTime()}
          style={{ width: "100%" }}
          // If we add steps, the tickmark will disappear. But it would be the best thing to have pre defined steps when dragging the slider
          // step={step}
          value={animationTime.getTime()}
          onChange={this.handleSetAnimationTime}
        />
        <datalist id="tickmarks">
          <option value={originalTime.getTime()}></option>
        </datalist>
        <div style={{ flexDirection: "row" }}>
          <button
            onClick={this.handleToStart}
            disabled={animationTime === startTime}
          >
            To Start
          </button>
          <button
            onClick={this.handleBackward}
            disabled={animationTime === startTime}
          >
            Backward
          </button>
          {isPlaying ? (
            <button onClick={this.handlePause}>Pause</button>
          ) : (
            <button onClick={this.handlePlay}>Play</button>
          )}
          <button
            onClick={this.handleToOriginal}
            disabled={animationTime === originalTime}
          >
            To original
          </button>
          <button
            onClick={this.handleForward}
            disabled={animationTime === endTime}
          >
            Forward
          </button>
          <button
            onClick={this.handleToEnd}
            disabled={animationTime === endTime}
          >
            To End
          </button>
        </div>
        Animated: {animationTime.toISOString()},<br />
        Original:
        {originalTime.toISOString()}
      </div>
    );
  };
}

// function TimeControl({originalTime, animationTime, startTime, endTime, interval, duration}) {

// }

// export default TimeControl
